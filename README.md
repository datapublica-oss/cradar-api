# Sidetrade Smart Explorer API

## Sidetrade Smart Explorer

Sidetrade Smart Explorer is a B2B marketing predictive business growth engine.

With Sidetrade Smart Explorer, find your next customers, discovering new opportunities thanks to B2B predictive marketing. Form start-up to big account users.

## API

Sidetrade Smart Explorerr application expose many APIs allowing users to fetch structured data, related to their account, such as company data or company lists.

### Java client

Java client is built under [Gradle](https://gradle.org/), using [Feign](https://github.com/OpenFeign/feign/) client.

You don't need to install Gradle on your own environment, since a [Gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) is already versionned.

#### Build last Java client release

```
$ git clone git@gitlab.com:sidetrade-oss/smart-explorer-api.git && \
    cd smart-explorer-api/java/ && \
    git checkout $(git tag | tail -1) && \
    ./gradlew build
```

#### Build last Java client release and publish it on your own Maven repo

```
$ git clone git@gitlab.com:sidetrade-oss/smart-explorer-api.git && \
    cd smart-explorer-api/java/ && \
    git checkout $(git tag | tail -1) && \
    ./gradlew build && \
    ./gradlew publish -PmavenReleaseRepo='http://domain.tld/repositories/releases' -PmavenRepoUsername='username' -PmavenRepoPassword='password'
```

#### Maven

Declare our OpenSource repository.

```xml
<repository>
    <id>sidetrade-oss</id>
    <name>Sidetrade OpenSource repository</name>
    <url>http://nexus.data-publica.com/content/repositories/public/</url>
</repository>
```

Add the API client dependency.

```xml
<dependency>
    <groupId>net.sidetrade.salesmarketing.smartexplorer.api</groupId>
    <artifactId>api-v4-client</artifactId>
    <version>4.31</version>
</dependency>
```

#### Usage

```java
// Init client
SmartExplorerClient client = SmartExplorerClient.connect("username", "password");

// Alternative: init with another URL
SmartExplorerClient client = SmartExplorerClient.connect("username", "password", "https://api-sales-marketing.sidetrade.net/explore/");


// Get company data
SdkCompany datapublica = client.getCompany("FR-533735932");
assert datapublica.identity.officialName.equals("DATA PUBLICA");

// Get Multiple companies
Set<String> companyIds = Stream.of("FR-533735932", "FR-430007252").collect(Collectors.toSet());
final SdkCompanySet companies = client.getCompanies(companyIds);
assert companies.items.size() == 2;

// Get relevant text from website
String crawl = client.getCompanyCrawl("FR-533735932");
assert crawl.contains("LA START UP DU MARKETING PRÉDICTIF B2B");

// Get my C-Radar lists
SdkListInfo list = client.getLists().iterator().next();
assert list.id.equals("1234567890AZERTYUIOP");
assert list.name.equals("Big data");

// Get companies registered into a list
SdkPaging<SdkListItem> items = client.getListItems("1234567890AZERTYUIOP", 1);
assert items.totalResults == 12;
assert items.nextPage == null;
assert items.results.iterator().next().companyId.equals("FR-533735932");

// Search for companies
final SdkSearchQuery query = new SdkSearchQuery();
query.query = "big data";
query.page = 1;
//
SdkPaging<SdkSearchResult> search = client.search(query);
assert search.totalResults == 28850;
assert search.nextPage == 2;
//
SdkSearchResult result = search.results.iterator().next();
assert result.name.equals("DATA PUBLICA");
assert result.description.equals("Comment identifier vos segments & vos cibles avec précision et complétude grâce au Big Data à la Data Science: système marketing et ventes prédictives B2B");

// Look for similar companies
SdkPaging<SdkSearchResult> similar = client.getSimilar("FR-533735932");
assert similar.totalResults == 129;
assert similar.results.iterator().next().name.equals("CORPORAMA");

// Look for similar companies to a group of companies
SdkPaging<SdkSearchResult> similar = client.getSimilar("FR-533735932", "FR-521286443");
assert similar.results.iterator().next().name.equals("KS KONCEPT");

// Get keywords for a list of companies
Map<String, Double> keywords = client.getMatrixKeywords(Stream.of("FR-533735932", "FR-521286443").collect(Collectors.toSet()), null, 10, null, null);
assert keywords.size() == 10;
assert keywords.containsKey("data");

// Get a slice of the companies matrix
Map slice = client.getMatrixSlice(Stream.of("FR-533735932", "FR-521286443").collect(Collectors.toSet()), SliceQuery.Format.JSON, false);

// Get companies linked to a given one
List<SdkLinkedCompany> linkedCompanies = client.getLinkedCompanies("FR-533735932");
```
