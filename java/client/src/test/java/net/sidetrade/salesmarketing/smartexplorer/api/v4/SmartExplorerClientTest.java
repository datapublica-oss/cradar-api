package net.sidetrade.salesmarketing.smartexplorer.api.v4;

import feign.Response;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.SdkPaging;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.SdkCompany;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.lists.SdkListInfo;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.lists.SdkListItem;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.search.SdkSearchQuery;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.search.SdkSearchResult;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.Year;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmartExplorerClientTest {

    private static SmartExplorerClient client;

    @Test
    public void getCompany() {
        //
        final SdkCompany company = client.getCompany("FR-533735932");
        //
        Assert.assertEquals("FR-533735932", company.id);
        Assert.assertEquals("FR", company.country);
        Assert.assertEquals("533735932", company.localId);
        Assert.assertEquals("Comment identifier vos segments & vos cibles avec précision et complétude grâce au Big Data à la Data Science: système marketing et ventes prédictives B2B", company.identity.description);
        Assert.assertEquals(1, company.financialData.size());
        Assert.assertEquals(Long.valueOf(232569), company.financialData.get(Year.of(2012)).revenue);
        Assert.assertNull(company.patents);
    }

    @Test
    public void getCompanyCrawl() {
        //
        final String crawl = client.getCompanyCrawl("FR-533735932");
        final String[] lines = crawl.split("\n");
        //
        Assert.assertEquals(303348, crawl.length());
        Assert.assertEquals(3400, lines.length);
        Assert.assertEquals("C-Radar, Le système marketing prédictif B2B", lines[0]);
        Assert.assertEquals("Contactez-nous", lines[3399]);
    }

    @Test
    public void getLists() {
        //
        final List<SdkListInfo> lists = client.getLists();
        //
        Assert.assertEquals(2, lists.size());
        //
        Assert.assertEquals("58201a48eb8bc10001f6ff69", lists.get(0).id);
        Assert.assertEquals("Data Loiret", lists.get(0).name);
        Assert.assertNull(lists.get(0).description);
        Assert.assertTrue(lists.get(0).shared);
        //
        Assert.assertEquals("58c659a9aac18100012bf4af", lists.get(1).id);
        Assert.assertEquals("Twitter sync", lists.get(1).name);
        Assert.assertEquals("Tests pour la synchronisation Twitter", lists.get(1).description);
        Assert.assertFalse(lists.get(1).shared);
    }

    @Test
    public void getListsItems() {
        //
        final SdkPaging<SdkListItem> paging = client.getListItems("58201a48eb8bc10001f6ff69", 1);
        //
        Assert.assertEquals(Long.valueOf(1), paging.totalResults);
        Assert.assertNull(paging.nextPage);
        Assert.assertEquals(Integer.valueOf(1), paging.pageMax);
        Assert.assertEquals(1, paging.results.size());
        //
        Assert.assertEquals("FR-533735932", paging.results.get(0).companyId);
        Assert.assertEquals(Double.valueOf(0), paging.results.get(0).customScore);
    }

    @Test
    public void search() {
        //
        final SdkSearchQuery query = new SdkSearchQuery();
        query.query = "www.c-radar.com";
        final SdkPaging<SdkSearchResult> results = client.search(query);
        //
        Assert.assertEquals(Long.valueOf(5), results.totalResults);
        Assert.assertNull(results.nextPage);
        Assert.assertEquals(Integer.valueOf(1), results.pageMax);
        Assert.assertEquals(5, results.results.size());
        //
        Assert.assertEquals("FR-533735932", results.results.get(0).id);
        Assert.assertEquals("DATA PUBLICA", results.results.get(0).name);
        Assert.assertEquals("FR-520075060", results.results.get(1).id);
        Assert.assertEquals("GLS", results.results.get(1).name);
        Assert.assertEquals("FR-339845976", results.results.get(2).id);
        Assert.assertEquals("RIALLAND", results.results.get(2).name);
        Assert.assertEquals("FR-789905247", results.results.get(3).id);
        Assert.assertEquals("KS KONCEPT", results.results.get(3).name);
        Assert.assertEquals("FR-512726373", results.results.get(4).id);
        Assert.assertEquals("INNOCHERCHE", results.results.get(4).name);
    }

    //

    private static final Charset CHARSET = StandardCharsets.UTF_8;

    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String CONTENT_TYPE_TEXT = "text/plain";

    @BeforeClass
    public static void init() {
        // Init SmartExplorer client (use mocks)
        client = SmartExplorerClient.connect("fakeUsername", "fakePassword", (request, options) -> {
            // Get target resource
            final URL resource = getMockResource(request.url());
            if(resource == null) {
                throw new IllegalStateException("Non mocked resource [" + request.url() + "]");
            }
            // Init headers
            final Map<String, Collection<String>> headers = new HashMap<>();
            headers.put("Content-Type", new ArrayList<>());
            // Add content type
            if(resource.toExternalForm().endsWith(".json")) {
                headers.get("Content-Type").add(CONTENT_TYPE_JSON);
            } else if(resource.toExternalForm().endsWith(".txt")) {
                headers.get("Content-Type").add(CONTENT_TYPE_TEXT);
            }
            // Mock response
            return Response.builder()
                    .status(200)
                    .headers(headers)
                    .body(IOUtils.toString(resource, CHARSET), CHARSET)
                    .build();
        });
    }

    private static final Pattern REGEX_COMPANY = Pattern.compile("^https://api-sales-marketing\\.sidetrade\\.net/explore/v4/companies/FR-([^/]+?)$");
    private static final Pattern REGEX_CRAWL = Pattern.compile("^https://api-sales-marketing\\.sidetrade\\.net/explore/v4/companies/FR-([^/]+?)/crawl$");
    private static final Pattern REGEX_LISTS = Pattern.compile("^https://api-sales-marketing\\.sidetrade\\.net/explore/v4/lists$");
    private static final Pattern REGEX_LIST_ITEMS = Pattern.compile("^https://api-sales-marketing\\.sidetrade\\.net/explore/v4/lists/([^/]+?)/items\\?page=([0-9]+)$");
    private static final Pattern REGEX_SEARCH = Pattern.compile("^https://api-sales-marketing\\.sidetrade\\.net/explore/v4/companies/search$");

    private static URL getMockResource(String url) {
        // Company
        {
            final Matcher matcher = REGEX_COMPANY.matcher(url);
            if (matcher.find()) {
                return SmartExplorerClientTest.class.getResource("/mock/companies/FR-" + matcher.group(1) + ".json");
            }
        }
        // Crawl
        {
            final Matcher matcher = REGEX_CRAWL.matcher(url);
            if (matcher.find()) {
                return SmartExplorerClientTest.class.getResource("/mock/crawls/FR-" + matcher.group(1) + ".txt");
            }
        }
        // Lists
        {
            final Matcher matcher = REGEX_LISTS.matcher(url);
            if (matcher.find()) {
                return SmartExplorerClientTest.class.getResource("/mock/lists/root.json");
            }
        }
        // List items
        {
            final Matcher matcher = REGEX_LIST_ITEMS.matcher(url);
            if (matcher.find()) {
                return SmartExplorerClientTest.class.getResource("/mock/lists/" + matcher.group(1) + "_p" + matcher.group(2) + ".json");
            }
        }
        // SEARCH
        {
            final Matcher matcher = REGEX_SEARCH.matcher(url);
            if (matcher.find()) {
                return SmartExplorerClientTest.class.getResource("/mock/search/root.json");
            }
        }
        //
        throw new IllegalStateException("Unsupported mock resource [" + url + "] !");
    }
}
