package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.search;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkSearchQuery {

    @JsonProperty(required = false)
    @JsonPropertyDescription("The search query")
    public String query;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The list filters")
    public SdkSearchFilter lists;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The country filters")
    public SdkSearchFilter countries;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The page number")
    public Integer page;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The restricted search field")
    public Field field;

    //

    public static enum Field {
        ALL,
        NAME;
    }
}
