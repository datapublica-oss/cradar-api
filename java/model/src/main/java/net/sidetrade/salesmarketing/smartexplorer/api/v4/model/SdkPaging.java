package net.sidetrade.salesmarketing.smartexplorer.api.v4.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.List;

public class SdkPaging<T> {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The total number of results")
    public Long totalResults;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The next page number, if applicable")
    public Integer nextPage;
    @JsonProperty(required = true)
    @JsonPropertyDescription("The last page number")
    public Integer pageMax;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The results for the current page")
    public List<T> results;
}
