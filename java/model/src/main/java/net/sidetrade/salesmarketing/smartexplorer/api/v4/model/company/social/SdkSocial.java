package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.social;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkSocial {

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company Facebook account")
    public SdkSocialAccount facebook;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company Twitter account")
    public SdkSocialAccount twitter;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company LinkedIn account")
    public SdkSocialAccount linkedIn;
}
