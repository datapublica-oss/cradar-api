package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.lists;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkListItem {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The company identifier")
    public String companyId;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company custom score for the target list")
    public Double customScore;
}
