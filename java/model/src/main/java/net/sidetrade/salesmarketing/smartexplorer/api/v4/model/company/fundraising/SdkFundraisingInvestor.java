package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.fundraising;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkFundraisingInvestor {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The investor identifier")
    public String id;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The investor name")
    public String name;
}
