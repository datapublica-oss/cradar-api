package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.search;

import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.SdkClassification;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.lists.SdkListInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.net.URL;
import java.util.List;

public class SdkSearchResult {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The company id")
    public String id;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The company name")
    public String name;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company description")
    public String description;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company classification (ex: NAF for France)")
    public SdkClassification classification;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company location")
    public SdkLocation location;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company thumbnail (social account picture)")
    public URL thumb;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The user's lists where company is present")
    public List<SdkListInfo> lists;

    //

    public static class SdkLocation {

        @JsonProperty(required = false)
        @JsonPropertyDescription("The location city")
        public String city;

        @JsonProperty(required = false)
        @JsonPropertyDescription("The location county (ex: departement name for France)")
        public String county;

        @JsonProperty(required = false)
        @JsonPropertyDescription("The location country code")
        public String country;
    }
}