package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.rd;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkCollaborativeProjectMember {

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company identifier of the member, if known")
    public String id;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The member label (company, research structure, etc)")
    public String name;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The member website URL")
    public String website;
}
