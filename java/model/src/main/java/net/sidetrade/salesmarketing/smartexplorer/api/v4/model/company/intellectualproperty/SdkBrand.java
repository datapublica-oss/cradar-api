package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.intellectualproperty;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.Date;
import java.util.Map;

public class SdkBrand {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The brand identifier")
    public String id;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The brand name")
    public String name;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The brand categories")
    public Map<String, String> categories;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The date when brand whas deposited")
    public Date depositDate;
}
