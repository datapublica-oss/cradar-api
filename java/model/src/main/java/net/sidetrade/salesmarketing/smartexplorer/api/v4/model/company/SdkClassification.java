package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkClassification {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The classification type")
    public Type type;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The classification identifier")
    public String code;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The classification name")
    public String name;

    //

    public enum Type {
        @JsonPropertyDescription("NAF classification (France)")
        NAF,
        @JsonPropertyDescription("NACEBEL classification (Belgium)")
        NACEBEL,
        @JsonPropertyDescription("SIC classification (UK)")
        SIC;
    }
}
