package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.contact.SdkContact;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.filing.SdkFiling;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.financial.SdkFinancialData;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.fundraising.SdkFundraising;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.intellectualproperty.SdkBrand;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.intellectualproperty.SdkPatent;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.rd.SdkCollaborativeProject;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.rd.SdkRDBadge;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.social.SdkSocial;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.website.SdkWebsite;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.lists.SdkListInfo;

import java.time.Year;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;

public class SdkCompany {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The company id")
    public String id;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The company registration country")
    public String country;
    @JsonProperty(required = true)
    @JsonPropertyDescription("The company id in the target registration country (ex: SIREN for France)")
    public String localId;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The basic company info (name, entity type, etc)")
    public SdkIdentity identity;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company head office")
    public SdkOffice headOffice;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The company secondary offices")
    public List<SdkOffice> secondaryOffices;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company financial data (revenue, operating income, employees count, etc)")
    public SortedMap<Year, SdkFinancialData> financialData;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company filings (ex: BODACC publications for France)")
    public List<SdkFiling> filings;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The company owned brands")
    public List<SdkBrand> brands;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The company owned patents")
    public List<SdkPatent> patents;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company website")
    public SdkWebsite website;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company social accounts")
    public SdkSocial social;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company categories (B2B, B2C, e-commerce, etc)")
    public List<SdkCategory> categories;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company fundraisings")
    public List<SdkFundraising> fundraisings;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company contacts (leaders, employees from social networks, etc)")
    public List<SdkContact> contacts;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company collaborative projects")
    public List<SdkCollaborativeProject> collaborativeProjects;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The company R&D distinctions (laureate, prizes, etc)")
    public List<SdkRDBadge> rdBadges;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The user's lists where company is referenced")
    public List<SdkListInfo> lists;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The activated permissions used to construct this bean")
    public Set<String> permissions;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The estimations of company headcount and revenue")
    public SdkEstimation estimation;

}
