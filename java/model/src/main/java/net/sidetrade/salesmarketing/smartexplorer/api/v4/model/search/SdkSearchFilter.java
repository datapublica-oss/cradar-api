package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.search;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.List;

public class SdkSearchFilter {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The filter operator")
    public Operator op;
    @JsonProperty(required = true)
    @JsonPropertyDescription("The filter values")
    public List<String> values;

    //

    public enum Operator {
        @JsonPropertyDescription("The field should contain all the values provided (AND)")
        all,
        @JsonPropertyDescription("The field should contain one of the values provided (OR)")
        any,
        @JsonPropertyDescription("The field should contain none of the values provided")
        none,
        @JsonPropertyDescription("The field should not contain all the values provided")
        not_all,
        @JsonPropertyDescription("The field should be present")
        exists;
    }
}
