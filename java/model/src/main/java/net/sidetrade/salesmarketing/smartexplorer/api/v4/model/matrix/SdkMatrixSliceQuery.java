package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.matrix;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.Set;

public class SdkMatrixSliceQuery {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The target companies list")
    public Set<String> companyIds;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The list of requested data types.")
    public Set<DataType> columnsType;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The list of requested languages for text data.")
    public Set<String> langs;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The minimum number of documents words have to appear in.")
    public Integer minDF;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The slice output format")
    public Format format;

    @JsonProperty(required = false)
    @JsonPropertyDescription("If true, slice should be generated using Python2 version")
    public boolean usePython2;

    //

    public static enum Format {
        JSON,
        MATRIX;
    }

    public static enum DataType {
        TEXT,
        NAF,
        STATUS,
        PATENT,
        FACEBOOK,
        CERTIFICATION,
        BRAND,
        CATALOGS,
        SOCIAL;
    }
}
