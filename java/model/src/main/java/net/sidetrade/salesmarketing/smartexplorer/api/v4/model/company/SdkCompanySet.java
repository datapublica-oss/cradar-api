package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company;

import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.Set;

/**
 * SdkCompany
 */
public class SdkCompanySet {

    @JsonPropertyDescription("Number of companies requested")
    public int nbCompanyRequested;

    @JsonPropertyDescription("Number of companies found")
    public int nbCompanyFound;

    @JsonPropertyDescription("The companies returned")
    public Set<SdkCompany> items;

    @JsonPropertyDescription("Companies Not found - Companies ids in format 'CountryCode-LocalId' ex: UK-4303002020")
    public Set<String> companiesNotFound;
}
