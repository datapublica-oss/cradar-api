package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.linkedcompanies;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class SdkLinkedCompany {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The linked company id")
    public String id;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The linked company name")
    public String name;

    @JsonProperty(required = true)
    @JsonPropertyDescription("True if the linked company is the owner of the target company, false otherwise")
    public boolean isOwner;

    @JsonProperty(required = true)
    @JsonPropertyDescription("True if the linked company has at least one common owner with the target company, false otherwise")
    public boolean hasCommonOwner;

    @JsonProperty(required = true)
    @JsonPropertyDescription("True if the linked company is a subsidiary of the target company, false otherwise")
    public boolean isSubsidiary;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The list of the common leader names between the linked company and the target one")
    public List<String> commonLeaders;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The list of the R&D collaboration types between the linked company and the target one")
    public Map<SdkRDCollaborationType, Integer> rdProjects;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The list of the outlinks types between the linked company website and the target company one")
    public Set<SdkOutlinkDirection> outlinks;

    //

    public enum SdkRDCollaborationType {
        @JsonPropertyDescription("Linked and target companies published some patents together")
        PATENT,
        @JsonPropertyDescription("Linked and target companies worked on some R&D collaborative projects together")
        PROJECT,
        @JsonPropertyDescription("Linked and target companies published a R&D publication together")
        PUBLICATION,
        @JsonPropertyDescription("Linked company is a spin-off company of the target one")
        SPINOFF
    }

    public static enum SdkOutlinkDirection {
        @JsonPropertyDescription("Target company website contains an outgoing link to the linked company website")
        IN,
        @JsonPropertyDescription("Linked company website contains an outgoing link to the target company website")
        OUT;
    }
}
