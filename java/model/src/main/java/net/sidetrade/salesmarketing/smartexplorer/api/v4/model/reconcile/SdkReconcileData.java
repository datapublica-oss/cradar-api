package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.reconcile;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.List;
import java.util.Objects;

public class SdkReconcileData {


    @JsonProperty(required = false)
    @JsonPropertyDescription("The company identifier used in the relevant country (siren/siret in France," +
            "company number in the UK, ...).")
    public String localId;
    @Deprecated
    @JsonProperty(required = false)
    @JsonPropertyDescription("The siren number (you should use localId instead)")
    public String siren;
    @Deprecated
    @JsonProperty(required = false)
    @JsonPropertyDescription("The siret number (you should use localId instead)")
    public String siret;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The company name")
    public String name;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The company country")
    public String country;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The company city")
    public String city;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The company zipcode")
    public String zipcode;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The company address")
    public String address;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The company website")
    public String website;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The company naf code")
    public String naf;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The facebook account URL")
    public String facebookURL;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The twitter account URL")
    public String twitterURL;
    @JsonProperty(required = false)
    @JsonPropertyDescription("Mail of a company's employee (domain name will be extracted if relevant)")
    public String mail;
    @JsonProperty(required = false)
    @JsonPropertyDescription("URL of the company's LinkedIn account")
    public String linkedInURL;
    @JsonProperty(required = false)
    @JsonPropertyDescription("Company's vat number")
    public String vat;
    @JsonProperty(required = false)
    @JsonPropertyDescription("Company's phone numbers, as a list of {phones}")
    public List<String> phones;
    @JsonProperty(required = false)
    @JsonPropertyDescription("Company's contacts as a list of {first name, last name}")
    public List<String> leaders;


    public SdkReconcileData() {
    }

    public SdkReconcileData(String siren, String siret, String name, String country, String city, String zipcode,
                            String address, String website, String naf, String facebookURL, String twitterURL,
                            String linkedInURL, String vat, List<String> leaders, List<String> phones) {
        this.siren = siren;
        this.siret = siret;
        this.name = name;
        this.country = country;
        this.city = city;
        this.zipcode = zipcode;
        this.address = address;
        this.website = website;
        this.naf = naf;
        this.facebookURL = facebookURL;
        this.twitterURL = twitterURL;
        this.linkedInURL = linkedInURL;
        this.vat = vat;
        this.leaders = leaders;
        this.phones = phones;
    }

    public SdkReconcileData(String siren, String siret, String name, String country, String city, String zipcode,
                            String address, String website, String naf, String facebookURL, String twitterURL, String linkedInURL,
                            String vat, List<String> leaders, List<String> phones, String localId) {
        this(siren, siret, name, country, city, zipcode, address, website, naf, facebookURL, twitterURL, linkedInURL,
             vat, leaders, phones);
        this.localId = localId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdkReconcileData that = (SdkReconcileData) o;
        return Objects.equals(siren, that.siren) &&
                Objects.equals(siret, that.siret) &&
                Objects.equals(name, that.name) &&
                Objects.equals(country, that.country) &&
                Objects.equals(city, that.city) &&
                Objects.equals(address, that.address) &&
                Objects.equals(zipcode, that.zipcode) &&
                Objects.equals(website, that.website) &&
                Objects.equals(naf, that.naf) &&
                Objects.equals(facebookURL, that.facebookURL) &&
                Objects.equals(twitterURL, that.twitterURL) &&
                Objects.equals(mail, that.mail) &&
                Objects.equals(localId, that.localId) &&
                Objects.equals(linkedInURL, that.linkedInURL) &&
                Objects.equals(phones, that.phones) &&
                Objects.equals(vat, that.vat) &&
                Objects.equals(leaders, that.leaders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(siren, siret, name, country, city, zipcode, address, website, naf, facebookURL, twitterURL, mail, localId, linkedInURL, vat, phones, leaders);
    }
}
