package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.fundraising;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.net.URL;
import java.util.Date;

public class SdkFundraisingNews {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The news URL")
    public URL url;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The news title")
    public String title;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The news content (raw summary text)")
    public String content;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The news publication date")
    public Date date;
}
