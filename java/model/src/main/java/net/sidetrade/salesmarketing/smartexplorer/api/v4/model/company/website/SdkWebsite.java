package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.website;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class SdkWebsite {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The website URL")
    public String url;
    @JsonProperty(required = true)
    @JsonPropertyDescription("The website domain")
    public String domain;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The most frequent words on the website, with their TF.IDF, computed over the whole corpus.")
    public Map<String, Float> keywords;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The emails extracted from this website")
    public LinkedHashSet<String> emails;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The phone numbers extracted from this website")
    public LinkedHashSet<String> phones;

    @JsonProperty(required = false)
    @JsonPropertyDescription("A list of the technologies detected for this website")
    public Set<String> technologies;
}
