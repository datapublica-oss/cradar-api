package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.social;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.net.URL;
import java.util.Date;
import java.util.SortedMap;

public class SdkSocialAccount {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The account identifier on the target social platform")
    public String id;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The social account username")
    public String username;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The social account profile URL")
    public URL profile;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The social account thumb URL")
    public URL thumb;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The social account creation date")
    public Date creationDate;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The fans history (likes for Facebook, followers for Twitter)")
    public SortedMap<String, Long> fans;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The current fans growth (harmonic mean)")
    public Double fansGrowth;
}